(function () {
    var myConnector = tableau.makeConnector();
    myConnector.getSchema = function (schemaCallback) {
        var regex = /\./gi;
        var cols = []
        $.getJSON('http://localhost:2540/oneNested/getTitle/coffee/village', function (data) {
            for (var i = 0, len = data['data']['title'].length; i < len; i++) {
                cols.push({
                    id: data['data']['title'][i].replace(regex, ' '),
                    alias: data['data']['title'][i].replace(regex, ' '),
                    dataType: tableau.dataTypeEnum.string
                })
            }

        })
            var tableSchema = {
                id: "coffee",
                alias: "coffee",
                columns: cols
            };
            schemaCallback([tableSchema]);
        }

    myConnector.getData = function(table, doneCallback) {
        $.getJson("http://localhost:2540/oneNested/coffee/village", function (json, status) {
            table.appendRows(json['data']);
            doneCallback();
        });
    }

    tableau.registerConnector(myConnector);

    $(document).ready(function() {
        $("#submit").click(function() {
            tableau.connectionData = $('#selectedYear').val()
            tableau.connectionName = "coffee" + tableau.connectionData;

            console.log('your message');
            tableau.submit();
        });
    });

})();

