(function () {
    // Create the connector object
    var myConnector = tableau.makeConnector();
    myConnector.getSchema = function (schemaCallback) {
        var cols = []
        console.log(tableau.connectionData)
        $.getJSON('http://localhost:2540/oneNested/getTitle/' + tableau.connectionData, function (data) {
            for (var i = 0, len = data['data']['title'].length; i < len; i++) {
                cols.push({
                    id: data['data']['title'][i],
                    alias: data['data']['title'][i],
                    dataType: tableau.dataTypeEnum.string
                })
            }
            var tableSchema = {
                id: tableau.connectionData.split("/")[1],
                alias: tableau.connectionData.split("/")[1],
                columns: cols
            };
            schemaCallback([tableSchema]);
        })
    };


    myConnector.getData = function (table, doneCallback) {
        $.getJSON("http://localhost:2540/oneNested/" + tableau.connectionData, function (json, status) {
            table.appendRows(json['data']);
            doneCallback();
        });
    }

    tableau.registerConnector(myConnector);

    $(document).ready(function () {
        //รับซื้อกาแฟ
        $("#SelCoffeeB").click(function () {
            tableau.connectionData = 'coffee/' + $('#coffeeGroupBy').val()+"/"+$('#yearCoffee').val()
            console.log(tableau.connectionData)
            tableau.connectionName = tableau.connectionData;
            
            tableau.submit();
        })
        //ชาน้ำมัน
        $("#selYearOilTeaB").click(function () {
            tableau.connectionData = "oilTea/" + $('#oilTeaGroupBy').val()
            tableau.connectionName = tableau.connectionData;
            tableau.submit();
        })
        //การปักกิ่งต้นชาน้ำมัน
        $("#selYearBranchOilTeaB").click(function () {
            tableau.connectionData = "oilTea/branch"+ $('#selectedYearBranchOilTea').val()
            tableau.connectionName = tableau.connectionData;
            tableau.submit();
        })
        //ขนส่งกาแฟ
        $("#selTransportB").click(function () {
            tableau.connectionData = "transport/" + $('#transportGroupBy').val()
            tableau.connectionName = tableau.connectionData;
            tableau.submit();
        })
        // สุ่มตรวจคุณภาพกาแฟ
        $("#selquality_coffeeB").click(function () {
            tableau.connectionData = 'qualityCoffee/' + $("#qualityGroupBy").val()
            tableau.connectionName = tableau.connectionData
            tableau.submit();
        })
        // ต้นแมคคาเดเมีย
        $("#selYearMacadamiaByB").click(function () {
            tableau.connectionData = 'macadamia/' + $('#MacadamiagroupBy').val()
            tableau.connectionName = tableau.connectionData
            tableau.submit();
        })
    });

})();
