(function() {
    // Create the connector object
    var myConnector = tableau.makeConnector();
  
    // Define the schema
    myConnector.getSchema = function(schemaCallback) {
        var cols = [{
            id: "_id",
            dataType: tableau.dataTypeEnum.string
        }, {
            id: "nameOwner",
            alias: "nameOwner",
            dataType: tableau.dataTypeEnum.string
        }, {
            id: "lastnameOwner",
            alias: "lastnameOwner",
            dataType: tableau.dataTypeEnum.string
        }, {
            id: "village",
            alias: "village",
            dataType: tableau.dataTypeEnum.string
        },
        {
            id: "fieldId",
            alias: "fieldId",
            dataType: tableau.dataTypeEnum.string
        },
        {
            id: "numberOiltea",
            alias: "numberOiltea",
            dataType: tableau.dataTypeEnum.string
        },
        {
            id: "isoDate",
            alias: "isoDate",
            dataType: tableau.dataTypeEnum.string
        },{
            id: "gradeOiltea",
            alias: "gradeOiltea",
            dataType: tableau.dataTypeEnum.string
        },
        {
            id: "heightOiltea",
            alias: "heightOiltea",
            dataType: tableau.dataTypeEnum.string
        },
        {
            id: "diameterOiltea",
            alias: "diameterOiltea",
            dataType: tableau.dataTypeEnum.string
        },
        {
            id: "canopy",
            alias: "canopy",
            dataType: tableau.dataTypeEnum.string
        }];
  
        var tableSchema = {
            id: "oiltea" + tableau.connectionData,
            alias: "OilTea" + tableau.connectionData,
            columns: cols
        };
  
        schemaCallback([tableSchema]);
    };
  
    // Download the data
    myConnector.getData = function(table, doneCallback) {
        // $.getJSON("https://jsonplaceholder.typicode.com/posts", function(posts) {
        $.getJSON("http://128.199.219.17:1112/oilTea/" + tableau.connectionData, function(data) {
            var data = data.data
            var tableData = [];
  
            // Iterate over the JSON object
            for (var i = 0, len = data.length; i < len; i++) {
                tableData.push({
                    "_id": data[i]._id,
                    "nameOwner": data[i].nameOwner,
                    "lastnameOwner": data[i].lastnameOwner,
                    "village": data[i].village,
                    "fieldId": data[i].fieldId,
                    "numberOiltea": data[i].numberOiltea,
                    "isoDate": data[i].isoDate,
                    "gradeOiltea": data[i].gradeOiltea,
                    "heightOiltea": data[i].heightOiltea,
                    "diameterOiltea": data[i].diameterOiltea,
                    "canopy": data[i].canopy
                });
            }
  
            table.appendRows(tableData);
            doneCallback();
        });
    };
  
    tableau.registerConnector(myConnector);
  
    $(document).ready(function() {
        $("#submit").click(function() {
            tableau.connectionData = $('#selectedYear').val()
            tableau.connectionName = "Oil Tea" + tableau.connectionData; // This will be the data source name in Tableau
            tableau.submit(); // This sends the connector object to Tableau
        });
    });
  
  })();
  